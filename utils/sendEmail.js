const nodemailer = require("nodemailer");
const { host, port, auth } = require("../config").email;

const transport = nodemailer.createTransport({ host, port, auth });

const sendEmail = async function sendEmail({ to, subject, html, from = host }) {
  await transport.sendMail({ from, to, subject, html });
};

module.exports = { sendEmail };
