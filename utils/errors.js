class ApiError extends Error {
  constructor({
    status = 500,
    code,
    message,
    target = undefined,
    details = undefined,
  }) {
    super(message);
    this.statusCode = status;
    this.code = code;
    this.message = message;
    this.target = target;
    this.details = details;
  }

  toJSON() {
    const { code, message, details, target } = this;
    return {
      code,
      message,
      details,
      target,
    };
  }
}

class BadRequestError extends ApiError {
  constructor({ message, target, details }) {
    super({ status: 400, code: "BadArgument", message, target, details });
  }
}

class UnauthorizedError extends ApiError {
  constructor(message, details) {
    super({ status: 401, code: "Unauthorized", message, details });
  }
}

class ForbiddenError extends ApiError {
  constructor({ message = "Access denied.", target }) {
    super({ status: 403, code: "Forbidden", message, target });
  }
}

class MalformedValueError extends ApiError {
  constructor({ message, details, target }) {
    super({ status: 400, code: "MalformedValue", message, target, details });
  }
}

class InvalidTokenError extends ApiError {
  constructor({ message, target, details }) {
    super({ status: 400, code: "InvalidToken", message, target, details });
  }
}

module.exports = {
  ApiError,
  BadRequestError,
  UnauthorizedError,
  ForbiddenError,
  MalformedValueError,
  InvalidTokenError,
};
