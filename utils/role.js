const Role = {
  Admin: "admin",
  User: "user",
};

module.exports = { Roles: Role };
