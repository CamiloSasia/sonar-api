const {
  CLIENT_URL,
  JWT_SECRET,
  SALT_ROUNDS,
  EMAIL_HOST,
  EMAIL_PORT,
  EMAIL_USER,
  EMAIL_PASS,
} = process.env;

module.exports = {
  clientUrl: CLIENT_URL,
  auth: {
    jwtSecret: JWT_SECRET,
    saltRounds: SALT_ROUNDS || 10,
  },
  email: {
    host: EMAIL_HOST,
    port: EMAIL_PORT,
    auth: {
      user: EMAIL_USER,
      pass: EMAIL_PASS,
    },
  },
};
