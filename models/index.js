/**
 * Connects to the database and exports the models.
 */
const mongoose = require("mongoose");

const User = require("./User");

mongoose.connect(process.env.DB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true, // To avoid DeprecationWarning: collection.ensureIndex is deprecated.
});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error:"));
db.once("open", () => {
  console.log("Successfully connected to the database\n");
});

module.exports = { User };
