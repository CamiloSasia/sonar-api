const { Schema, model } = require("mongoose");
const crypto = require("crypto");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const config = require("../config");
const { sendEmail } = require("../utils/sendEmail");
const { UnauthorizedError } = require("../utils/errors");

const SALT_ROUNDS = config.auth.saltRounds;

const userSchema = new Schema(
  {
    name: {
      type: String,
      trim: true,
      require: true,
    },
    email: {
      type: String,
      trim: true,
      unique: true,
      lowercase: true,
      required: true,
    },
    password: {
      type: String,
      select: false, // Don't return the password.
      required: true,
    },
    role: {
      type: String,
      enum: ["user", "admin"],
      default: "user",
    },
    resetToken: {
      token: String,
      expires: Date,
      // expires: { type: String, select: false },
      // expires: { type: Date, select: false },
    },
  },
  { timestamps: true } // Automatically add createdAt and updatedAt.
);

userSchema.pre("save", async function () {
  if (this.isModified("password")) {
    this.password = await bcrypt.hash(this.password, SALT_ROUNDS);
  }
});

userSchema.statics.authenticate = async function (email, password) {
  const user = await User.findOne({ email }).select("+password");

  if (!user || !(await bcrypt.compare(password, user.password))) {
    throw new UnauthorizedError({ message: "Invalid email or password" });
  }

  delete user.password;

  return user;
};

userSchema.methods.generateAuthToken = function () {
  const { id, name, email, role } = this;
  const token = jwt.sign({ id, name, email, role }, config.auth.jwtSecret, {
    expiresIn: 60 * 60 * 24, // Expire in 24 hours.
  });
  return token;
};

userSchema.methods.generatePasswordResetToken = async function () {
  const resetToken = crypto.randomBytes(32).toString("hex");
  const hash = await bcrypt.hash(resetToken, config.auth.saltRounds);

  // The token expires after 24 hours.
  this.resetToken = {
    token: hash,
    expires: new Date(Date.now() + 24 * 60 * 60 * 1000),
  };
  await this.save();

  return resetToken;
};

userSchema.methods.comparePassword = async function (password) {
  const user = await User.findById(this._id).select("+password");
  return await bcrypt.compare(password, user.password);
};

userSchema.methods.comparePasswordResetToken = async function (token) {
  return await bcrypt.compare(token, this.resetToken.token);
};

userSchema.methods.sendEmail = async function (subject, html) {
  await sendEmail({ to: this.email, subject, html });
};

const User = model("User", userSchema);

module.exports = User;
