const authRoutes = require("./authRoutes");
const accountRoutes = require("./accountRoutes");
const {
  loginRequired,
  currentUserRequired,
  roleRequired,
} = require("../middlewares/authMiddleware");

module.exports = (app) => {
  app.get("/", (req, res) => {
    res.json({ status: "Ok" });
  });

  app.use("/auth", authRoutes);
  app.use("/account", accountRoutes);

  app.get("/secret", loginRequired, (req, res) => {
    return res.send({ message: "You made it!" });
  });

  app.get("/user/:id", currentUserRequired, (req, res) => {
    return res.json({ message: "You made it!" });
  });

  app.get("/admin", roleRequired("admin"), (req, res) => {
    return res.json({ message: "Welcome, admin!" });
  });
};
