const express = require("express");
const Joi = require("joi");

const { User } = require("../models");
const config = require("../config");
const {
  BadRequestError,
  MalformedValueError,
  InvalidTokenError,
} = require("../utils/errors");

const router = express.Router();

/**
 * POST /auth/register
 */
router.post("/register", async (req, res, next) => {
  const { email } = req.body;

  if (await User.findOne({ email })) {
    return next(new BadRequestError({ message: "User already exists" }));
  }

  try {
    const user = await User.create(req.body);
    const { password, ...userWithoutPassword } = user;

    return res.status(201).json({ userWithoutPassword });
  } catch (error) {
    next(new Error(error));
  }
});

/**
 * POST /auth/login
 */
router.post("/login", async (req, res, next) => {
  const {
    error,
    value: { email, password },
  } = validateLogin(req.body);

  try {
    if (error) {
      let errorMessage;

      if (error.details.length === 1) {
        errorMessage = error.message;
      } else {
        errorMessage = "Multiple errors in login data";
      }

      const badRequestError = new BadRequestError({
        message: errorMessage,
        details: error.details.map(
          (detail) =>
            new MalformedValueError({
              message: detail.message,
              target: detail.context.key,
            })
        ),
      });

      throw badRequestError;
    }

    const user = await User.authenticate(email, password);
    const token = user.generateAuthToken();

    // TODO: No enviar email en el token.
    return res.json({
      id: user.id,
      name: user.name,
      email: user.email,
      role: user.role,
      token,
    });
  } catch (error) {
    return next(error);
  }
});

router.post(
  "/forgot-password",
  (req, res, next) => {
    const { error } = validateForgotPassword(req.body);

    if (error) {
      const badRequestError = new BadRequestError({
        message: error.message,
        target: error.details[0].context.key,
      });
      return next(badRequestError);
    }
    return next();
  },
  async (req, res, next) => {
    const { email } = req.body;

    try {
      const user = await User.findOne({ email });

      // Always return ok response to prevent email enumeration.
      if (!user) return res.status(200).end();

      // Generate token and store it on database.
      const resetToken = await user.generatePasswordResetToken();

      // Send email.
      const resetUrl = `${config.clientUrl}/reset-password?token=${resetToken}&id=${user.id}`;
      await user.sendEmail(
        "Password Reset",
        `<h4>Reset Password Email</h4>
<p>Please reset your password by clicking here: <a href=${resetUrl}>link</a>.</p>
<p>This link will be valid for 1 day.</p>`
      );

      return res.json(resetUrl);
    } catch (error) {
      next(error);
    }
  }
);

// TODO: Quitar el id de los parámetros de la URL.
// Cambiar la llamada en el front-end.
router.post("/reset-password", async (req, res, next) => {
  const { userId, token, password } = req.body;

  try {
    const user = await User.findById(userId);
    const passwordResetToken = user.resetToken;

    if (
      !passwordResetToken.token || // Token already used.
      passwordResetToken.expires < new Date() || // Expired token.
      !(await user.comparePasswordResetToken(token)) // Invalid token.
    ) {
      throw new InvalidTokenError({
        message: "Invalid or expired password reset token",
      });
    }

    // Update the user’s account with the new password.
    user.password = password;
    await user.save();

    await user.sendEmail(
      "Password Reset Successfully",
      "<p>Password Reset Successfully<p>"
    );

    // Delete the token from the database.
    user.resetToken = undefined;
    await user.save();

    res.json({ message: "Password reset successful, you can now login" });
  } catch (error) {
    return next(error);
  }
});

function validateLogin(req) {
  const schema = Joi.object({
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(5).max(255).required(),
  });
  // Return all errors found.
  return schema.validate(req, { abortEarly: false });
}

function validateForgotPassword(req) {
  const schema = Joi.object({
    email: Joi.string().email().required(),
  });
  return schema.validate(req, { abortEarly: false });
}

module.exports = router;
