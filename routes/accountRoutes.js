const express = require("express");
const Joi = require("joi");

const { loginRequired } = require("../middlewares/authMiddleware");
const Role = require("../utils/role");
const {
  ForbiddenError,
  BadRequestError,
  MalformedValueError,
} = require("../utils/errors");

const router = express.Router();

router.put(
  "/:id",
  loginRequired,
  async (req, res, next) => {
    // Users can update their own account and admins can update any account.
    if (req.user.role === Role.Admin || req.params.id === req.user.id) {
      return next();
    }
    return next(new ForbiddenError({}));
  },
  async (req, res, next) => {
    const {
      error,
      value: { oldPassword, newPassword },
    } = validate(req.body);

    try {
      if (error) {
        const badRequestError = new BadRequestError({
          message: error.message,
          details: error.details.map(
            (detail) =>
              new MalformedValueError({
                message: detail.message,
                target: detail.context.key,
              })
          ),
        });

        throw badRequestError;
      }

      const user = req.user;

      if (!(await user.comparePassword(oldPassword))) {
        throw new ForbiddenError({
          message: 'Invalid password',
          target: "oldPassword",
        });
      }

      user.password = newPassword;
      await user.save();

      return res.status(201).end();
    } catch (error) {
      next(error);
    }
  }
);

function validate(req) {
  const schema = Joi.object({
    oldPassword: Joi.string().min(5).max(255).required(),
    newPassword: Joi.string().min(5).max(255).required(),
    confirmPassword: Joi.any()
      .equal(Joi.ref("newPassword"))
      .label("Confirm password")
      .messages({ "any.only": '"confirmPassword" does not match' })
      .required(),
  });
  // Return all errors found.
  return schema.validate(req, { abortEarly: false });
}

module.exports = router;
