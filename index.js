require("dotenv").config();

const express = require("express");
const morgan = require("morgan");

const routes = require("./routes");
const error = require("./middlewares/errorMiddleware");

const PORT = process.env.PORT || 3000;
const app = express();

app.use(express.json()); // Enable to access request body as req.body.
app.use(morgan("tiny")); // Enable incoming request logging in tiny format.

routes(app);

app.use(error);

app.listen(PORT, () => {
  console.log(`[${app.get("env")}] : Listening on http://localhost:${PORT}`);
  console.log("  Press CTRL-C to stop\n");
});
