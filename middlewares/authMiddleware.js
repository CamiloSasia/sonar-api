const jwt = require("jsonwebtoken");

const { User } = require("../models");
const config = require("../config");
const {
  UnauthorizedError,
  ForbiddenError,
  InvalidTokenError,
} = require("../utils/errors");

const SECRET = config.auth.jwtSecret;

function getToken(authHeader) {
  if (!authHeader) {
    throw new UnauthorizedError({ message: "No token provided." });
  }

  try {
    const authHeaderValue = authHeader.split(" ")[1];
    const token = jwt.verify(authHeaderValue, SECRET);

    return token;
  } catch (error) {
    throw new InvalidTokenError({ message: "Invalid access token." });
  }
}

module.exports = {
  loginRequired: async function (req, res, next) {
    try {
      const { email } = getToken(req.headers.authorization);
      req.user = await User.findOne({ email });
      return next();
    } catch (error) {
      return next(error);
    }
  },

  currentUserRequired: async function (req, res, next) {
    try {
      const { id } = getToken(req.headers.authorization);
      if (id === req.params.id) {
        req.user = await User.findById(id);
        return next();
      } else {
        throw new ForbiddenError({});
      }
    } catch (error) {
      return next(error);
    }
  },

  roleRequired: function (...role) {
    return function (req, res, next) {
      try {
        const token = getToken(req.headers.authorization);

        if (role.includes(token.role)) {
          return next();
        } else {
          throw new ForbiddenError({});
        }
      } catch (error) {
        return next(error);
      }
    };
  },
};
