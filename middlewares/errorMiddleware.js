const { ApiError } = require("../utils/errors");

module.exports = (error, req, res, next) => {
  // If you call next() with an error after you have started writing the response,
  // for example, when an error occurs while streaming the response to the client,
  // the Express default error handler closes the connection and fails the request.
  // For further information, visit https://expressjs.com/es/guide/error-handling.html
  if (res.headersSent) {
    return next(err);
  }

  let err = error;
  if (!(error instanceof ApiError)) {
    err = new ApiError({
      status: 500,
      code: "InternalServerError",
      message: "An unexpected error ocurred",
    });
  }

  error instanceof ApiError
    ? console.log(JSON.stringify(error))
    : console.log(error.stack);

  return res.status(err.statusCode).json({ error: err.toJSON() });
};
